use crate::{catalog_catalog::CatalogCatalog, loadable::JsonFile};
use catalog_catalog::DownloadableAssetInfo;
use chrono::{SecondsFormat, Utc};
use reqwest::blocking::Client;
use std::{
    env,
    fs::{self, File},
    io,
    path::Path,
    process::ExitCode,
    thread,
    time::Duration,
};
pub mod catalog_catalog;
pub mod file_list;
pub mod loadable;

const SLEEP_BETWEEN_REQUESTS: u64 = 10;

#[derive(Debug)]
pub enum Platform {
    Android,
    IOS,
}

impl TryFrom<&str> for Platform {
    type Error = ();
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "Android" => Ok(Self::Android),
            "iOS" => Ok(Self::IOS),
            _ => Err(()),
        }
    }
}

impl Platform {
    pub fn to_string_in_url(&self) -> String {
        match self {
            Platform::Android => "Android",
            Platform::IOS => "iOS",
        }
        .to_string()
    }
    pub fn to_dirname(&self) -> String {
        match self {
            Platform::Android => "Android",
            Platform::IOS => "iOS",
        }
        .to_string()
    }

    pub fn url_to_platform_dir(&self, resource_url: &str) -> String {
        format!("{}/{}", resource_url.replace("http://", "https://"), self.to_string_in_url(),).to_string()
    }

    pub fn url_to_file(&self, resource_url: &str, file: &str) -> String {
        format!("{}/{}/{}", resource_url.replace("http://", "https://"), self.to_string_in_url(), file).to_string()
    }
}

fn progress_bar(value: i64, max: i64, total_chars: u64) -> String {
    const BLOCK_CHARACTERS: [&str; 9] = [" ", "▏", "▎", "▍", "▌", "▋", "▊", "▉", "█"];

    let progress = if max == 0 { 1.0 } else { (value as f64 / max as f64).clamp(0.0, 1.0) };

    let total_units = total_chars * 8;
    let unit_progress = (progress * (total_units as f64)) as u64;

    let mut progress_bar = String::new();

    let full_blocks = unit_progress / 8;
    for _ in 0..full_blocks {
        progress_bar += BLOCK_CHARACTERS[8];
    }

    if full_blocks < total_chars {
        let block_fragment = unit_progress % 8;
        progress_bar += BLOCK_CHARACTERS[block_fragment as usize];

        let empty_blocks = total_chars - full_blocks - 1;
        for _ in 0..empty_blocks {
            progress_bar += BLOCK_CHARACTERS[0];
        }
    }

    let percentage = progress * 100.0;
    return format!("{value:>4} [{progress_bar}] {max:<4} {percentage:>5.1}%");
}

const STATUS_FILENAME: &str = "status.txt";

fn write_status(status: &str) {
    if let Err(e) = fs::write(STATUS_FILENAME, status) {
        eprintln!("warning: could not write to '{STATUS_FILENAME}': {e:?}");
    }
}

fn download_file(client: &Client, url: &str, output_file: &Path) {
    println!("Creating empty file...");
    if let Some(output_dir) = output_file.parent() {
        fs::create_dir_all(output_dir).expect("Could not create directory");
    }
    let mut file = File::create(output_file).expect("Could not create file");

    println!("Sending request...");
    let mut response = client.get(url).send().expect("Request failed");
    let status_code = response.status().as_u16();
    println!("Received response: status {}, content length: {:?}", status_code, response.content_length());
    if status_code == 200 {
        io::copy(&mut response, &mut file).expect("Failed to save file");
    } else {
        println!("Response text: {:#?}", response.text());
        panic!("Status code was not 200, exiting");
    }
}

fn download_catalog_catalog(client: &Client, resource_url: &str, platform: &Platform, output_file: &Path) {
    download_file(&client, &CatalogCatalog::get_url_for_platform(resource_url, platform), output_file);
}

fn download_missing_bundle(client: &Client, url: &str, output_file: &Path) {
    if output_file.is_file() {
        println!("Bundle '{}' already downloaded, skipping", output_file.to_str().unwrap());
        return;
    }

    println!("Waiting to download bundle '{url}'...");
    thread::sleep(Duration::from_secs(SLEEP_BETWEEN_REQUESTS));

    download_file(client, url, output_file);
}

fn read_or_download_catalog_catalog(client: &Client, resource_url: &str, platform: &Platform, catalog_catalog_path: &Path) -> CatalogCatalog {
    println!("'catalog_catalog.json' path: {catalog_catalog_path:?}");
    if !catalog_catalog_path.is_file() {
        println!("Downloading 'catalog_catalog.json'...");
        download_catalog_catalog(&client, &resource_url, &platform, catalog_catalog_path);
    } else {
        println!("File 'catalog_catalog.json' already downloaded, reading from file");
    }

    CatalogCatalog::from_file(catalog_catalog_path).expect("Failed to load 'catalog_catalog.json'")
}

fn download_all_assets(resource_url: &str, platform: &Platform, archive_dirname_bundles: &Path, archive_dirname_version: &Path) {
    let client = Client::new();

    let catalog_catalog = read_or_download_catalog_catalog(&client, resource_url, platform, &archive_dirname_version.join("catalog_catalog.json"));
    let file_list = catalog_catalog.get_file_list(resource_url, &platform);
    let file_count = file_list.files.len();
    println!("Found {file_count} downloadable asset bundles in catalog_catalog.json");
    file_list
        .to_file(&archive_dirname_version.join("file_list.json"), true)
        .expect("Could not save file list");

    let missing_files: Vec<&DownloadableAssetInfo> = file_list
        .files
        .iter()
        .filter(|downloadable_asset_info| {
            let filename = &downloadable_asset_info.filename;
            let output_file = &archive_dirname_bundles.join(&filename);
            return !output_file.is_file(); // If the files are not there, return true to keep them in the missing_files variable
        })
        .collect();
    let missing_file_count = missing_files.len();
    let downloaded_file_count = file_count - missing_file_count;

    println!("{downloaded_file_count}/{file_count} files are already downloaded, {missing_file_count} are missing");

    for (file_number, downloadable_asset_info) in missing_files.iter().enumerate() {
        let url = &downloadable_asset_info.correct_url;
        let filename = &downloadable_asset_info.filename;

        let bar = progress_bar(file_number as i64, missing_file_count as i64, 20);
        write_status(&format!(
            "Downloading missing bundle for **{platform:?}**: `{bar}` [total bundles: {file_count}]"
        ));

        download_missing_bundle(&client, &url, &archive_dirname_bundles.join(&filename));
    }
}

fn run_download(resource_url: &str, platform_name: &str, archive_root_dirname: &Path) {
    let platform = Platform::try_from(platform_name).expect("Unknown platform name");

    let version_name = resource_url.split("/").last().expect("Could not get version name from url");

    let archive_dirname = archive_root_dirname.join("platforms");
    let archive_dirname_platform = archive_dirname.join(platform.to_dirname());
    let archive_dirname_bundles = archive_dirname_platform.join("bundles");
    let archive_dirname_version = archive_dirname_platform.join(version_name);

    println!("Running downloader for:\n- resource_url: {resource_url}\n- platform: {platform:?}");
    write_status(&format!("Started downloading for {platform:?}"));
    download_all_assets(&resource_url, &platform, &archive_dirname_bundles, &archive_dirname_version);
    write_status(&format!("Finished downloading for {platform:?}"));
}

fn main() -> ExitCode {
    let args: Vec<String> = env::args().collect();

    if args.len() < 3 {
        eprintln!("usage: {} <resource_url> <platform> [platform...]", args[0]);
        return ExitCode::from(2);
    }

    let current_datetime = Utc::now().to_rfc3339_opts(SecondsFormat::Secs, true);
    println!("================================");
    println!("rilzine-asset-downloader started at {current_datetime}");
    eprintln!("================================");
    eprintln!("rilzine-asset-downloader started at {current_datetime}");

    let archive_root_dirname = Path::new("archive");
    let archive_lock_path = archive_root_dirname.join("lockfile");

    // Check if archive is locked - if it is, exit
    if archive_lock_path.is_file() {
        eprintln!("error: archive is currently locked, try again later");
        return ExitCode::from(3);
    }

    // Lock the archive
    fs::create_dir_all(archive_lock_path.parent().unwrap()).unwrap();
    if let Err(e) = File::create(&archive_lock_path) {
        eprintln!("error: cannot lock the archive: {e:?}");
        return ExitCode::from(4);
    }

    // Run the downloader
    write_status("");
    for i in 2..args.len() {
        run_download(&args[1], &args[i], archive_root_dirname);
    }
    write_status("Done");

    // Unlock the archive
    if let Err(e) = fs::remove_file(archive_lock_path) {
        eprintln!("error: cannot unlock the archive: {e:?}");
        return ExitCode::from(5);
    }

    ExitCode::SUCCESS
}
