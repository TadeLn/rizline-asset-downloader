use std::{fs, path::Path};

use serde::{Deserialize, Serialize};

#[derive(Debug)]
pub enum LoadError {
    IoError(std::io::Error),
    SerdeJsonError(serde_json::Error),
}

impl From<std::io::Error> for LoadError {
    fn from(value: std::io::Error) -> Self {
        Self::IoError(value)
    }
}

impl From<serde_json::Error> for LoadError {
    fn from(value: serde_json::Error) -> Self {
        Self::SerdeJsonError(value)
    }
}

pub trait JsonFile {
    fn from_file(path: &Path) -> Result<Self, LoadError>
    where
        Self: Sized,
        Self: for<'a> Deserialize<'a>,
    {
        Self::from_string(&fs::read_to_string(path)?)
    }

    fn from_string(text: &str) -> Result<Self, LoadError>
    where
        Self: Sized,
        Self: for<'a> Deserialize<'a>,
    {
        Ok(serde_json::from_str(&text)?)
    }

    fn to_file(&self, path: &Path, pretty: bool) -> Result<(), LoadError>
    where
        Self: Serialize,
    {
        fs::write(path, self.to_string(pretty)?)?;
        Ok(())
    }

    fn to_string(&self, pretty: bool) -> Result<String, LoadError>
    where
        Self: Serialize,
    {
        Ok(if pretty {
            serde_json::to_string_pretty(&self)?
        } else {
            serde_json::to_string(&self)?
        })
    }
}
