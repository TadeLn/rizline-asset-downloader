use regex::Regex;
use serde::{Deserialize, Serialize};

use crate::{file_list::FileList, loadable::JsonFile, Platform};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ObjectType {
    #[serde(rename = "m_AssemblyName")]
    pub assembly_name: String,
    #[serde(rename = "m_ClassName")]
    pub class_name: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ProviderData {
    #[serde(rename = "m_Id")]
    pub id: String,
    #[serde(rename = "m_ObjectType")]
    pub object_type: ObjectType,
    #[serde(rename = "m_Data")]
    pub data: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CatalogCatalog {
    #[serde(rename = "m_LocatorId")]
    pub locator_id: String,
    #[serde(rename = "m_InstanceProviderData")]
    pub instance_provider_data: ProviderData,
    #[serde(rename = "m_SceneProviderData")]
    pub scene_provider_data: ProviderData,
    #[serde(rename = "m_ResourceProviderData")]
    pub resource_provider_data: Vec<ProviderData>,
    #[serde(rename = "m_ProviderIds")]
    pub provider_ids: Vec<String>,
    #[serde(rename = "m_InternalIds")]
    pub internal_ids: Vec<String>,
    #[serde(rename = "m_KeyDataString")]
    pub key_data_string: String,
    #[serde(rename = "m_BucketDataString")]
    pub bucket_data_string: String,
    #[serde(rename = "m_EntryDataString")]
    pub entry_data_string: String,
    #[serde(rename = "m_ExtraDataString")]
    pub extra_data_string: String,
    #[serde(rename = "m_resourceTypes")]
    pub resource_types: Vec<ObjectType>,
    #[serde(rename = "m_InternalIdPrefixes")]
    pub internal_id_prefixes: Vec<()>,
}

impl JsonFile for CatalogCatalog {}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct DownloadableAssetInfo {
    pub original_url: String,
    pub correct_url: String,
    pub filename: String,
}

impl DownloadableAssetInfo {
    pub fn create(internal_id: &str, resource_url: &str, platform: &Platform, remove_dot_bundle: bool) -> Self {
        // Convert a http://rizastcdn.pigeongames.cn/default url to an actually working one
        let unnecessary_url_part = format!("http://rizastcdn.pigeongames.cn/default/{}/", platform.to_string_in_url());

        let mut filename = internal_id.replace(&unnecessary_url_part, "");
        if remove_dot_bundle {
            // Remove ".bundle" at the end because correct urls for criaddressables don't have ".bundle" at the end
            filename = filename.replace(".bundle", "");
        }

        let correct_url = platform.url_to_file(resource_url, &filename);
        DownloadableAssetInfo {
            original_url: internal_id.to_owned(),
            correct_url,
            filename,
        }
    }
}

fn is_criaddressable(internal_id: &str, platform: &Platform) -> bool {
    internal_id.starts_with(&format!(
        "http://rizastcdn.pigeongames.cn/default/{}/cridata_assets_criaddressables/",
        platform.to_string_in_url()
    ))
}
fn is_normal_bundle(internal_id: &str, platform: &Platform) -> bool {
    internal_id.starts_with(&format!("http://rizastcdn.pigeongames.cn/default/{}/", platform.to_string_in_url()))
}

impl CatalogCatalog {
    pub fn get_url_for_platform(resource_url: &str, platform: &Platform) -> String {
        platform.url_to_file(resource_url, "catalog_catalog.json")
    }

    pub fn get_file_list(&self, resource_url: &str, platform: &Platform) -> FileList {
        let mut results = Vec::new();

        let regex_hex32 = Regex::new(r"^[0-9a-f]{32}$").unwrap();

        for internal_id in &self.internal_ids {
            if is_criaddressable(internal_id, &platform) {
                // These are the files with filenames that we need to remove the ".bundle" from
                results.push(DownloadableAssetInfo::create(internal_id, resource_url, &platform, true));
            } else if is_normal_bundle(internal_id, &platform) {
                // These are normal asset bundles
                results.push(DownloadableAssetInfo::create(internal_id, resource_url, &platform, false))
            } else if regex_hex32.is_match(&internal_id) {
                // Ignore hex32 ids for now
            } else {
                // Ignore other kinds of ids for now
            }
        }

        FileList { files: results }
    }
}
