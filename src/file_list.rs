use serde::{Deserialize, Serialize};

use crate::{catalog_catalog::DownloadableAssetInfo, loadable::JsonFile};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FileList {
    pub files: Vec<DownloadableAssetInfo>,
}

impl JsonFile for FileList {}
