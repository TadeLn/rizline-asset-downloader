#!/usr/bin/env python3
import sys
import json
import os
import shutil


# Copies all of the version's bundles to a given directory, based on file_list.json
def main(arguments):
    if len(arguments) < 3:
        print(f"usage: {arguments[0]} <file_list_json_path> <output_dir>")
        exit()

    file_list_json_path = os.path.abspath(arguments[1])
    bundles_dir = os.path.realpath(
        os.path.join(os.path.dirname(file_list_json_path), "..", "bundles")
    )
    output_dir = arguments[2]
    dry_run = arguments[3] == "dry" if len(arguments) > 3 else False

    if not dry_run:
        os.makedirs(output_dir, exist_ok=True)

    file_list = None
    with open(file_list_json_path, "r") as f:
        file_list = json.load(f)

    for file_info in file_list["files"]:
        filename = file_info["filename"]
        asset_path = os.path.join(bundles_dir, filename)
        output_path = os.path.realpath(os.path.join(output_dir, filename))
        print(
            f"Copying file: {file_info}\n  asset path: {asset_path}\n  output path: {output_path}"
        )
        if not dry_run:
            os.makedirs(os.path.dirname(output_path), exist_ok=True)
            shutil.copy2(asset_path, output_path)


if __name__ == "__main__":
    main(sys.argv)
